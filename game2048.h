#ifndef GAME2048_H
#define GAME2048_H

#include "core.h"
#include "arduino.h"

static const Point tl(110,260), br(555,705);
static const int keys[] = {KEY_LEFT_ARROW, KEY_UP_ARROW, KEY_RIGHT_ARROW, KEY_DOWN_ARROW};
static const int fieldSize = 4;
static const int maxPower = 12;

Rect findField(const Mat &screen, const vector<Mat> &patterns) {
    vector<vector<Rect> > matches (patterns.size());
    for (uint i = 0; i < matches[i].size(); ++i) {
        matches[i] = findPatternPositions(screen, patterns[i], i ? 0.02 : 0.2);
    }
    for (uint i = 0; i < matches.size(); ++i) {
        for (Rect j : matches[i]) {
            vector<int> field = vector<int>(fieldSize*fieldSize, 0);
            for (int x = 0; x < fieldSize; ++x) {
                for (int y = 0; y < fieldSize; ++y) {
                    Point guessCorner = Point(j.x, j.y) + Point(x*j.width, y*j.height);
                    for (uint n = 0; n < matches.size(); ++n) {
                        for (Rect m : matches[n]) {
                            if (distance(guessCorner, Point(m.x, m.y)) < (j.width + j.height)/8) {
                                field[x * fieldSize + y] = 1 << n;
                            }
                        }
                    }
                }
            }
            if (*min_element(field.begin(), field.end()) > 0) {
                return Rect(j.x, j.y, j.x + fieldSize*j.width, j.y + fieldSize*j.height);
            }
        }
    }
    return Rect();
}

vector<vector<int> > reconstructField(const Mat &screen, const vector<Mat> &patterns, const Point &tl, const Point &br) {
    double sideLenght = distance(tl, br) / sqrt(2.0) / fieldSize;
    vector<vector<int> > field(fieldSize, vector<int> (fieldSize));
    for (int x = 0; x < fieldSize; ++x) {
        for (int y = 0; y < fieldSize; ++y) {
            Rect guessCell = Rect(tl.x + x*sideLenght, tl.y + y*sideLenght, sideLenght, sideLenght);
            Mat mask = Mat(screen, guessCell);
            field[y][x] = 1 << bestPatternIndex(mask, patterns);
        }
    }
    return field;
}

vector<vector<int> > makeMoveLeft(const vector<vector<int> > &field) {
    vector<vector<int> > result = field;
    for (int i = 0; i < fieldSize; ++i) {
        for (int j = 0; j < fieldSize - 1; ++j) {
            if (result[i][j] > 1 && result[i][j] == result[i][j+1]) {
                result[i][j] *= 2;
                result[i][j+1] = 1;
            }
        }
        int k = 0;
        for (int j = 0; j < fieldSize; ++j) {
            if (result[i][j] > 1) {
                result[i][k++] = result[i][j];
            }
        }
        while (k < fieldSize) {
            result[i][k++] = 1;
        }
    }
    return result;
}

vector<vector<int> > makeMoveTo(const vector<vector<int> > &field, int dir) {
    vector<vector<int> > result = field;
    for (int i = 0; i < dir; ++i) {
        result = clockWiseRotateSquareMatrix(result);
    }
    result = makeMoveLeft(result);
    for (int i = 0; i < (4 - dir); ++i) {
        result = clockWiseRotateSquareMatrix(result);
    }
    return result;
}

double countScores1(const vector<vector<int> > &field) {
    double scores = 0;
    for (int i = 0; i < fieldSize; ++i) {
        for (int j = 0; j < fieldSize; ++j) {
            if (i < fieldSize - 1) {
                scores += abs(field[i][j] - field[i+1][j]);
            }
            if (j < fieldSize - 1) {
                scores += abs(field[i][j] - field[i][j+1]);
            }
        }
    }
    return scores;
}

double countScores2(const vector<vector<int> > &field) {
    double scores = 0;
    for (int i = 0; i < fieldSize; ++i) {
        for (int j = 0; j < fieldSize; ++j) {
            scores += field[i][j];
        }
    }
    return scores;
}

double countScores3(const vector<vector<int> > &field) {
    double scores = 0;
    for (int i = 0; i < fieldSize; ++i) {
        for (int j = 0; j < fieldSize; ++j) {
            if (field[i][j] == 1) {
                scores++;
            }
        }
    }
    return scores;
}

pair<int,double> findBestMoveDirection(const vector<vector<int> > &field, int depth = 2) {
    vector<double> scores(4);
    for (int dir = 0; dir < 4; ++dir) {
        vector<vector<int> > newField = makeMoveTo(field, dir);
        if (field == newField) {
            scores[dir] = 0;
        } else if (depth == 0) {
            scores[dir] = countScores3(newField);
        } else {
            int emptyCellsCount = 0;
            scores[dir] = 0;
            for (int i = 0; i < fieldSize; ++i) {
                for (int j = 0; j < fieldSize; ++j) {
                    if (newField[i][j] > 1) continue;
                    emptyCellsCount++;
                    newField[i][j] = 4;
                    scores[dir] += findBestMoveDirection(newField, depth - 1).second;
                    newField[i][j] = 2;
                    scores[dir] += findBestMoveDirection(newField, depth - 1).second;
                    newField[i][j] = 1;
                }
            }
            scores[dir] /= 2*emptyCellsCount;
        }
    }
    int bestDir = max_element(scores.begin(), scores.end()) - scores.begin();
    double bestScores = *max_element(scores.begin(), scores.end());
    return make_pair(bestDir, bestScores);
}

Mat play2048() {
    vector<Mat> patterns(maxPower);
    for (int i = 0; i < maxPower; ++i)
    {
        patterns[i] = getPatternByName("2048_" + int2str(1 << i));
    }
    Mat screen = makeScreenshot();
    vector<vector<int> > field = reconstructField(screen, patterns, tl, br);
    pair<int,double> result = findBestMoveDirection(field);
    cout << result.first << ' ' << result.second << endl << endl;
    cout << "FIELD:" << endl;
    cout << field;
    emulateKeyTap(keys[result.first]);
    rectangle(screen, tl, br, Scalar(0, 255, 0), 3);
    resize(screen, screen, Size(), 0.2, 0.2);
    return screen;
}

#endif // GAME2048_H
