#ifndef CAMCALIBRATION_H
#define CAMCALIBRATION_H

#include "core.h"
#include "camera.h"

static Size patternSize = Size(3, 4);
static Size resolution;
static vector<vector<Point2f> > imagePoints;
static vector<vector<Point3f> > objectPoints;
static int framesCount = 0;

bool addChessboardPoints(Mat &image) {
    resolution = Size(image.cols, image.rows);
    patternSize.width++;
    patternSize.height++;
    vector<Point2f> boardPoints(patternSize.width * patternSize.height);
    int flags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE | CALIB_CB_FILTER_QUADS | CALIB_CB_FAST_CHECK;
    bool chessboardFound = findChessboardCorners(image, patternSize, boardPoints, flags);
    drawChessboardCorners(image, patternSize, boardPoints, chessboardFound);
    if ((int)boardPoints.size() != patternSize.height * patternSize.width) {
        return false;
    }
    imagePoints.push_back(vector<Point2f>());
    objectPoints.push_back(vector<Point3f>());
    int index = 0;
    double centerX = double(patternSize.width - 1) / 2;
    double centerY = double(patternSize.height - 1) / 2;
    for (int j = 0; j < patternSize.height; ++j) {
        for (int k = 0; k < patternSize.width; ++k, ++index) {
            Point2f projectPoint (boardPoints[index].x, boardPoints[index].y);
            Point3f virtualPoint (double(k) - centerX, double(j) - centerY, 0);
            imagePoints.back().push_back(projectPoint);
            objectPoints.back().push_back(virtualPoint);
        }
    }
    framesCount++;
    return true;
}

void camCalibration() {
    Mat cameraMatrix = Mat::eye(3,3,CV_64F);
    Mat coeffs = Mat::zeros(5, 1, CV_64F);
    vector<Mat> rvecs(framesCount);
    vector<Mat> tvecs(framesCount);
    int flags = CV_CALIB_FIX_PRINCIPAL_POINT;
    double rms = calibrateCamera(objectPoints, imagePoints, resolution, cameraMatrix, coeffs, rvecs, tvecs, flags);
    DBG(rms);
    DBG(resolution);
    DBG(cameraMatrix);
    DBG(coeffs);
    for (int i = 0; i < framesCount; ++i)
    {
        DBG(rvecs[i]);
        DBG(tvecs[i]);
    }
    stopCamera();
}

static bool dogNail = false;
Mat makePhotoshot() {
    if (dogNail) {
        dogNail = false;
        waitKey();
    }

    Mat frame = getFrameFromCam();
    if (addChessboardPoints(frame)) {
        dogNail = true;
    }
    return frame;
}

#endif // CAMCALIBRATION_H
