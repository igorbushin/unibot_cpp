#ifndef SPOILER_H
#define SPOILER_H

#include <ctime>
#include <cassert>
#include <unistd.h>

#include <fstream>
#include <iostream>
#include <string>
#include <set>
#include <deque>
#include <functional>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
using namespace std;

#include <opencv2/opencv.hpp>
using namespace cv;

#include <QCoreApplication>
#include <QDebug>
#include <QImage>
#include <QString>
#include <QSerialPort>
#include <QSerialPortInfo>
QT_USE_NAMESPACE

#define CLOCKS_PER_SECOND 1e5
#define START(name) clock_t name = clock(); long long count_##name = 0;
#define COUNT(name) cout << #name << " = " << double(clock() - name) / CLOCKS_PER_SECOND << endl;
#define MEAN(name) count_##name++; cout << #name << " = " << double(clock() - name) / CLOCKS_PER_SECOND / count_##name << endl;
#define DBG(x) cout << #x << " = " << x << endl;

#endif // SPOILER_H
