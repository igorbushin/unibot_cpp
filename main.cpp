#include "game2048.h"
#include "game3inRow.h"
#include "featuressearch.h"
#include "camcalibration.h"

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);
    //infinityLoop("2048", play2048, 500, setupArduino, stopArduino);
    //infinityLoop("features search", featuresSearch, 100, setupCamera, stopCamera);
    infinityLoop("calibration", makePhotoshot, 10, setupCamera, camCalibration);
    exit(0);
    return a.exec(); //jff
}
