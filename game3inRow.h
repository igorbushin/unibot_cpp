#ifndef THREE_IN_ROW_H
#define THREE_IN_ROW_H

#include "core.h"

Mat play3inRow() {
    Mat screen = makeScreenshot();
    resize(screen, screen, Size(), 0.2, 0.2);
    return screen;
}

#endif // 3_IN_ROW_H
