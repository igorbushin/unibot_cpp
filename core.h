#ifndef CORE_H
#define CORE_H

#include "spoiler.h"

static unordered_map<string, Mat> patterns;

class Monster {
    vector<bool> vb;
    vector<int> vi;
    vector<string> vs;
    vector<Mat> vm;
public:
     Monster (...) {

     }
     void parse(...) {

     }
};

float distance(Point a, Point b) {
    float dx = a.x - b.x;
    float dy = a.y - b.y;
    return sqrt(dx*dx + dy*dy);
}

int str2int(const string &str) {
    stringstream ss(str);
    int val; ss >> val;
    return val;
}

string int2binStr(int val, int lenght) {
    lenght = max(lenght, 30);
    string result(lenght, '0');
    for (int i = 0; i < lenght; ++i) {
        if ((1 << i) & val) {
            result[i] = '1';
        }
    }
    reverse(result.begin(), result.end());
    return result;
}

string int2str(int val) {
    stringstream ss;
    ss << val;
    return ss.str();
}

ostream & operator << (ostream &os, const vector<vector<int> > &value) {
    for (uint i = 0; i < value.size(); ++i) {
        for (uint j = 0; j < value[i].size(); ++j) {
            os << value[i][j] << '\t';
        }
        os << endl;
    }
    return os;
}

QImage Mat2QImage(Mat const& src) {
    Mat tmp;
    cvtColor(src, tmp, CV_BGR2RGB); // cvtColor Makes a copy
    QImage dst((const uchar *) tmp.data, tmp.cols, tmp.rows, tmp.step, QImage::Format_RGB888);
    dst.bits(); // enforce deep copy
    return dst;
}

Mat QImage2Mat(QImage const& src)
{
    QImage tmp = src.convertToFormat(QImage::Format_RGB888);
    Mat dst(tmp.height(),tmp.width(),CV_8UC(tmp.depth()/8), (void*)tmp.bits(),tmp.bytesPerLine());
    dst = dst.clone();
    cvtColor(dst, dst, CV_RGB2BGR);
    return dst;
}

vector<vector<int> > clockWiseRotateSquareMatrix(const vector<vector<int> > &matrix) {
    vector<vector<int> > result = matrix;
    for (int i = 0; i < (int)result.size(); ++i) {
        assert(result.size() == result[i].size());
        for (int j = 0; j < (int)result.size(); ++j) {
            result[i][j] = matrix[j][result.size() - i - 1];
        }
    }
    return result;
}

Mat getPatternByName(string patternName) {
    string filePath = ":patterns/" + patternName + ".png";
    if (patterns.count(patternName) == 0) {
        QImage patternImage = QImage(QString::fromStdString(filePath));
        Mat patternMat = QImage2Mat(patternImage);
        patterns[patternName] = patternMat;
    }
    return patterns[patternName];
}

string executeCommandLine(const string &command) {
    string fileName = command;
    if (command.size() > 12) {
        fileName = command.substr(0, 6) + command.substr(command.size()-6);
    }
    for (int i = 0; i < (int)fileName.size(); ++i) {
        if(isalpha(fileName[i])) continue;
        fileName[i] = isspace(fileName[i]) ? '_' : '*';
    }
    bool done = !system((command + " > " + fileName).c_str());
    fstream fileStream(fileName, ios_base::in);
    string output = (done ? "" : "FAIL"), outputLine;
    while(getline(fileStream, outputLine)) {
        output = output + outputLine + "\r\n";
    }
    fileStream >> outputLine;
    fileStream.close();
    return output;
}

Mat makeScreenshot() {
    executeCommandLine("import -window root screen.png");
    return imread("screen.png");
}

void infinityLoop(const string &windowName, Mat(*iteration)(void), int delay, void(*setup)(void) = 0, void(*stop)(void) = 0) {
    setup();
    namedWindow(windowName);
    while(true) {
        imshow(windowName, iteration());
        int key = waitKey(delay);
        if (key == 27) break;
    }
    stop();
}

vector<Rect> findPatternPositions(const Mat &source, const Mat &pattern, float threshold) {
    Mat matchMap;
    matchTemplate(source, pattern, matchMap, TM_SQDIFF_NORMED);
    vector<Rect> matches;
    for (int i = 0; i < matchMap.rows; ++i) {
        for (int j = 0; j < matchMap.cols; ++j) {
            if (matchMap.at<float>(i, j) < threshold) {
                matches.push_back(Rect(j, i, pattern.cols, pattern.rows));
            }
        }
    }
    return matches;
}

int bestPatternIndex(const Mat &source, const vector<Mat> &patterns) {
    Mat matchMap;
    vector<double> result (patterns.size());
    for (uint i = 0; i < patterns.size(); ++i) {
        matchTemplate(source, patterns[i], matchMap, TM_SQDIFF_NORMED);
        double minVal, maxVal;
        minMaxLoc(matchMap, &minVal, &maxVal);
        result[i] = minVal;
    }
    return min_element(result.begin(), result.end()) - result.begin();
}

#endif
