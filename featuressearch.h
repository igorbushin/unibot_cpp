#ifndef FEATURESSEARCH_H
#define FEATURESSEARCH_H

#include "core.h"
#include "camera.h"

static Ptr<FastFeatureDetector> fastFeatureDetector = FastFeatureDetector::create(20, true);
static Mat prevFrame;
static vector<KeyPoint> prevKeyPoints;

vector<DMatch> featureTracking(const Mat frame1, const Mat frame2, vector<KeyPoint> &keyPoints1, vector<KeyPoint> &keyPoints2, vector<uchar> &status) {
    vector<Point2f> points1, points2;
    KeyPoint::convert(keyPoints1, points1);
    vector<float> err;
    calcOpticalFlowPyrLK(frame1, frame2, points1, points2, status, err);
    int indexCorrection = 0;
    for (int i = 0; i < (int)status.size(); i++) {
        Point2f point = points2.at(i - indexCorrection);
        if ((status.at(i) == 0) || (point.x < 0) || (point.y < 0)) {
            if((point.x < 0) || (point.y < 0)) {
                status.at(i) = 0;
            }
            points1.erase (points1.begin() + i - indexCorrection);
            points2.erase (points2.begin() + i - indexCorrection);
            indexCorrection++;
        }
    }
    KeyPoint::convert(points1, keyPoints1);
    KeyPoint::convert(points2, keyPoints2);
    assert(keyPoints1.size() == keyPoints2.size());
    vector<DMatch> matches(keyPoints1.size());
    for (int i = 0; i < (int)matches.size(); ++i) {
        matches[i] = DMatch(i, i, distance(points1[i], points2[i]));
    }
    sort(matches.begin(), matches.end());
    return matches;
}

Mat featuresSearch() {
    Mat curFrame = getFrameFromCam();
    cvtColor(curFrame, curFrame, CV_BGR2GRAY);
    if (!prevFrame.data || !prevKeyPoints.size()) {
        prevFrame = curFrame;
        fastFeatureDetector->detect(curFrame, prevKeyPoints);
    }

    vector<KeyPoint> curKeyPoints;
    vector<uchar> status;
    vector<DMatch> matches = featureTracking(curFrame, prevFrame, prevKeyPoints, curKeyPoints, status);

    Mat result;
    vector<char> mask(curKeyPoints.size(), 0);
    fill(mask.begin(), mask.begin() + min((int)mask.size(), 10), 1);
    drawMatches(prevFrame, prevKeyPoints, curFrame, curKeyPoints, matches, result, Scalar::all(-1), Scalar::all(-1), mask, DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    fastFeatureDetector->detect(curFrame, prevKeyPoints);
    prevFrame = curFrame;

    return result;
}

#endif // FEATURESSEARCH_H
