#ifndef CAMERA_H
#define CAMERA_H

#include "core.h"

static VideoCapture stream = VideoCapture();

void setupCamera() {
    stream.open(CAP_ANY);
}

void stopCamera() {
    stream.release();
}

Mat getFrameFromCam() {
    Mat frame;
    stream >> frame;
    return frame;
}

#endif // CAMERA_H
