TARGET = unbot
TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
QT += core serialport gui

SOURCES += main.cpp

INCLUDEPATH += -I/usr/local/include
LIBS += -L/usr/local/lib \
    -lopencv_shape \
    -lopencv_stitching \
    -lopencv_objdetect \
    -lopencv_superres \
    -lopencv_videostab \
    -lopencv_calib3d \
    -lopencv_features2d \
    -lopencv_highgui \
    -lopencv_videoio \
    -lopencv_imgcodecs \
    -lopencv_video \
    -lopencv_photo \
    -lopencv_ml \
    -lopencv_imgproc \
    -lopencv_flann \
    -lopencv_core

HEADERS += \
    spoiler.h \
    core.h \
    game2048.h \
    game3inRow.h \
    keyboard.h \
    arduino.h \
    featuressearch.h \
    camcalibration.h \
    camera.h

RESOURCES += \
    resources.qrc

DISTFILES += \
    todo.txt


