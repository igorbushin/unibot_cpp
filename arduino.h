#ifndef ARDUINO_H
#define ARDUINO_H

#include "keyboard.h"
#include "spoiler.h"

static QSerialPort serialPort;

void setupArduino() {
    QSerialPortInfo info("ttyACM0");
    qDebug() << "Name        : " << info.portName();
    qDebug() << "Manufacturer: " << info.manufacturer(); //if showing manufacturer, means Qstring &name is good
    qDebug() << "Busy: " << info.isBusy();
    serialPort.setPortName("ttyACM0");
    serialPort.setBaudRate(QSerialPort::Baud9600);
    serialPort.setDataBits(QSerialPort::Data8);
    serialPort.setParity(QSerialPort::NoParity);
    serialPort.setStopBits(QSerialPort::OneStop);
    serialPort.setFlowControl(QSerialPort::NoFlowControl);
    while(!serialPort.isOpen()) serialPort.open(QIODevice::ReadWrite);
    qDebug() << "Is open : " << serialPort.isReadable();
    qDebug() << "Is readable : " << serialPort.isReadable();
    qDebug() << "Is writable : " << serialPort.isWritable();
}

void emulateKeyTap(char key) {
    if (serialPort.isOpen() && serialPort.isWritable() && serialPort.isReadable()) {
        serialPort.write(&key, 1);
        serialPort.flush();
        qDebug() << "Done";
    }
    else
    {
        qDebug() << "An error occured";
    }
}

void stopArduino() {
    serialPort.close();
}

#endif // ARDUINO_H
